<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Adarsh Puraskar</title>

<?php include 'headerstyles.php' ?>

<!-- Fav Icons -->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>
<div class="page-wrapper">    
    <!-- Preloader -->
    <div class="preloader"></div> 
   <?php include 'header.php' ?>

    <!--main sub page -->
    <!--Page title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg)">
        <div class="container">
            <h1>Contact us</h1>
        </div>
    </section>

    <div class="bread-crumb">
        <div class="container">
            <ul class="clearfix">
                <li><a href="index.php"><span class="fa fa-home"></span>Home</a></li>
                <li class="active">Contact</li>
            </ul>
        </div>
    </div>
    <!--/ page title -->

    <!-- page body -->
    <div class="subpage">
     <!-- Announcement -->
     <section class="about-us-two">

                  
        
    <!-- Contact form -->
    <section class="contact-form-section">
          
        <div class="container">
            <?php
                    if(isset($_POST['submit'])) {

//                        echo "testet";
//                        exit;
                            $to = "raj.var@hotmail.com";
                            //$to = "sai96030@gmail.com";
                            $subject = "Contact Message from ".$_POST['form_name'];
                            
                            
                            $name = $_POST['form_name'];
                            $email = $_POST['form_email'];
                            $phone = $_POST['form_phone'];
                            $service = $_POST['form_services'];
                            $msg = $_POST['form_message'];
                            
                           
                            
                            $message = "
                        <html>
                        <head>
                        <title>Contact Form Send</title>
                        </head>
                        <body>
                       
                        <table>
                        <tr>
                            <td>Name : </td>
                            <td>".$name."</td>
                        </tr>
                         <tr>
                            <td>Email : </td>
                            <td>".$email."</td>
                        </tr>
                         <tr>
                            <td>Phone : </td>
                            <td>".$phone."</td>
                        </tr>
                         <tr>
                            <td>Services : </td>
                            <td>".$service."</td>
                        </tr>
                         <tr>
                            <td>Message : </td>
                            <td>".$msg."</td>
                        </tr>
                        </table>
                        </body>
                        </html>
                        ";

                        // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                        // More headers
                        $headers .= 'From: <info@adarsh.com>' . "\r\n";
                        #$headers .= 'Cc: myboss@example.com' . "\r\n";

                        if(mail($to, $subject, $message, $headers)){
                            
                            echo "Mail Send Successfully, we will contact you soon";
                        };
                    }
                    ?>
            <div class="row">
                <div class="col-lg-7 mb-30">
                    <div class="default-form-area">
                        <h1>Send A Message</h1>
                        <form id="contact-form" name="contact_form" class="contact-form style-five" action="" method="post">
                            <div class="row clearfix">
                                <div class="col-md-6 column">        
                                    <div class="form-group">
                                        <label for="">Name</label>
                                        <input type="text" name="form_name" class="form-control" placeholder="" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 column">
                                    <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="email" name="form_email" class="form-control required email"  placeholder="" required="">
                                    </div>
                                </div>
                                <div class="col-md-6 column">
                                    <div class="form-group">
                                        <label for="">Phone</label>
                                        <input type="text" name="form_phone" class="form-control"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6 column">
                                    <div class="form-group">
                                        <label for="">Subject</label>
                                        <input type="text" name="form_services" class="form-control"  placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12 column">
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="form_message" class="form-control textarea required" placeholder=""></textarea>
                                    </div>
                                </div>                                            
                            </div>
                            <div class="contact-section-btn">
                                <div class="form-group style-two">
                                    <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                                    <input class="theme-btn btn-style-two" type="submit" name="submit" data-loading-text="Please wait..." value="Submit Now">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-5 mb-30 pl-lg-5">
                    <div class="title">
                        <h4>Address Information</h4>                       
                    </div>
                    <ul class="contact-info-list">
                        <li><span class="fas fa-map-marker-alt"></span><b>Address</b>Flat no 313, Prithvi Block, My Home Nawadweepa, Hitech city, Hyderabad, Telangana</li>
                        <li><span class="fa fa fa-envelope-open"></span><b>Email</b>jmnr.srpfdn@gmail.com</li>
                        <li><span class="fa fa-phone"></span><b>Phone / Fax</b>+ (91) 9849297398</li>
                        <li><span class="fa fa-globe"></span><b>Website</b>www.adarshpuraskar.org</li>
                    </ul>
                    <!-- <ul class="social-icon-three">
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-google-plus-g"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                    </ul> -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    
                </div>
            </div>            
        </div>
    </section>
           
    </section>
    <!--/ Announcment-->
    <!--/ page body -->

    <!--main sub page -->
    
   <?php include 'footer.php' ?>
</div>
<!--End pagewrapper-->
    

<!-- Scroll Top Button -->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-angle-up"></span>
</button>   

<?php include 'footerscripts.php' ?>

</body>
</html>