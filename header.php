 <!-- main header -->
    <header class="main-header">        
        <!--Header-Upper-->
        <div class="header-upper">
            <div class="container clearfix">                    
                <div class="float-left logo-outer">
                    <div class="logo"><a href="index.php"><img src="images/logo.png" alt="Adarsh Puraskar" title="Adarsh Puraskar"></a></div>
                </div>                
                <div class="float-right upper-right clearfix">                    
                    <div class="nav-outer clearfix">
                       <!-- Main Menu -->
                       <nav class="main-menu navbar-expand-lg">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        
                        <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                                <li class="current"><a href="index.php">Home</a></li>
                                <li><a href="about.php">Foundation</a></li>                                                                   
                                <li><a href="announcement.php">Announcements</a></li>
                                <li><a href="media.php">Media</a></li>
                                <li><a href="contact.php">Contact</a></li>
                            </ul>
                        </div>
                    </nav>                        
                    <!-- Main Menu End-->
                    </div>                    
                </div>
                    
            </div>
        </div>
        <!--End Header Upper-->

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="container">
                <div class="clearfix">
                    <!--Logo-->
                    <div class="logo float-left">
                        <a href="javascript:void(0)" class="img-responsive"><img src="images/logo.png" alt="Adarsh Puraskar" title=""></a>
                    </div>
                    
                    <!--Right Col-->
                    <div class="right-col float-right">
                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">
                                <!-- Toggle Button -->      
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="index.php">Home</a></li>
                                <li><a href="about.php">Foundation</a></li>                                                                   
                                <li><a href="announcement.php">Announcements</a></li>
                                <li><a href="media.php">Media</a></li>
                                <li><a href="contact.php">Contact</a></li>
                            </ul>
                            </div>
                        </nav>                        
                        <!-- Main Menu End-->
                    </div>
                </div>  
            </div>
        </div>
        <!--End Sticky Header-->
    </header>