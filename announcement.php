<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Adarsh Puraskar</title>

<?php include 'headerstyles.php' ?>

<!-- Fav Icons -->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>
<div class="page-wrapper">    
    <!-- Preloader -->
    <div class="preloader"></div> 
   <?php include 'header.php' ?>

    <!--main sub page -->
    <!--Page title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg)">
        <div class="container">
            <h1>Announcements</h1>
        </div>
    </section>

    <div class="bread-crumb">
        <div class="container">
            <ul class="clearfix">
                <li><a href="index.php"><span class="fa fa-home"></span>Home</a></li>
                <li class="active">Announcements</li>
            </ul>
        </div>
    </div>
    <!--/ page title -->

    <!-- page body -->
    <div class="subpage">
     <!-- Announcement -->
     <section class="about-us-two">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
               
                <!-- <div class="col-lg-6">
                    <img src="images/resource/invitation.jpg" alt="" class="img-fluid">
                </div> -->
                
                <?php

                if(isset($_POST['submit'])) {

//                        echo "testet";
//                        exit;
                            $to = "raj.var@hotmail.com";
                            //$to = "sai96030@gmail.com";
                            $subject = "Contact Message from ".$_POST['form_name'];
                            
                            
                            $pname = $_POST['form_name'];
                            $pemail = $_POST['form_email'];
                            $pmobile = $_POST['form_mobnumber'];
                            $paddress = $_POST['form_address'];
                          
                          
                            $pa_name = $_POST['form_p_name'];
                            $pa_email = $_POST['form_p_email'];
                            $pa_mobile = $_POST['form_p_number'];
                            $pa_address = $_POST['form_p_address'];
                            
                            $par_dob = $_POST['form_datepicker'];
                            $par_state = $_POST['form_state'];
                            $par_other_awards = $_POST['form_others'];
                            $par_reasons = $_POST['form_reasons'];
                            $par_works = $_POST['form_works'];
                            
                           
                            
                            $message = "
                        <html>
                        <head>
                        <title>Contact Form Send</title>
                        </head>
                        <body>
                       
                        <table>
                        <tr>
                        <th>Personal Information</th>
                        </tr>
                        <br/>
                        <tr>
                            <td>Name : </td>
                            <td>".$pname."</td>
                        </tr>
                         <tr>
                            <td>Email : </td>
                            <td>".$pemail."</td>
                        </tr>
                         <tr>
                            <td>Phone : </td>
                            <td>".$pmobile."</td>
                        </tr>
                         <tr>
                            <td>Address : </td>
                            <td>".$paddress."</td>
                        </tr>
                        <br/>
                        <tr>
                         <th>Proposed awardee Details</th>
                        </tr>
                        <br/>
                        <tr>
                            <td>Name of the proposed awardee : </td>
                            <td>".$pa_name."</td>
                        </tr>
                         <tr>
                            <td>Email : </td>
                            <td>".$pa_email."</td>
                        </tr>
                         <tr>
                            <td>Mobile Number : </td>
                            <td>".$pa_mobile."</td>
                        </tr>
                         <tr>
                            <td>Address : </td>
                            <td>".$pa_address."</td>
                        </tr>
                          <br/>
                        <tr>
                        <th>Awardee Registration</th>
                        </tr>
                        <tr>
                            <td>Date of Birth : </td>
                            <td>".$par_dob."</td>
                        </tr>
                         <tr>
                            <td>State : </td>
                            <td>".$par_state."</td>
                        </tr>
                         <tr>
                            <td>Other awards & honours received earlier : </td>
                            <td>".$par_other_awards."</td>
                        </tr>
                         <tr>
                            <td>Reasons to consider for this award : </td>
                            <td>".$par_reasons."</td>
                        </tr>
                        <tr>
                            <td>His works/achievements : </td>
                            <td>".$par_works."</td>
                        </tr>
                        </table>
                        </body>
                        </html>
                        ";

                        // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                        // More headers
                        $headers .= 'From: <info@adarsh.com>' . "\r\n";
                        #$headers .= 'Cc: myboss@example.com' . "\r\n";

                        if(mail($to, $subject, $message, $headers)){
                            
                            echo "Mail Send Successfully, we will contact you soon";
                        }
                    }
                    ?>

                <!-- col -->
                <div class="col-lg-12 pb-3">
                    <h3 class="text-center pb-4">Send Nominations for “Aadarsh Puraskar” 2020 </h3>

                    <!-- row -->
                    <form enctype="multipart/form-data" class="contact-form style-five" method="post" action="">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4>Personal Information</h4>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="form_name" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Mobile Number</label>
                                    <input type="text" name="form_mobnumber" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Email Address</label>
                                    <input type="text" name="form_email" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-12 column">        
                                <div class="form-group">
                                    <label for="">Address</label>
                                    <input type="text" name="form_address" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h4>Proposed awardee Details </h4>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Name of the proposed awardee </label>
                                    <input type="text" name="form_p_name" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Mobile Number</label>
                                    <input type="text" name="form_p_number" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" name="form_p_email" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-12 column">        
                                <div class="form-group">
                                    <label for="">Address</label>
                                    <input type="text" name="form_p_address" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <h4>Awardee Registration  </h4>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Date of Birth</label>
                                    <input id="datepicker" name="form_datepicker" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">State </label>
                                    <select name="form_state" class="form-control" style="height:auto;">
                                        <option>Telangana</option>
                                        <option>Andhra Pradesh</option>
                                        <option>Maharastra</option>
                                        <option>Tamilnadu</option>
                                        <option>Karnataka</option>
                                        <option>Kerala</option>
                                        <option>Goa</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Other awards & honours received earlier  </label>
                                    <select name="form_others" class="form-control" style="height:auto;">
                                        <option>Yes</option>
                                        <option>No</option>                                       
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">Reasons to consider for this award</label>
                                    <input type="text" name="form_reasons" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <div class="col-md-4 column">        
                                <div class="form-group">
                                    <label for="">His works/achievements </label>
                                    <input type="text" name="form_works" class="form-control" value="" placeholder="" required="">
                                </div>
                            </div>
                            <!--div class="col-md-4 column">        
                                <div class="form-group file">
                                    <label for="">Attachments  </label>
                                    <input type="file" name="form_attachment"  class="form-control" value="" placeholder="" required="">
                                </div>
                            </div-->

                            <div class="col-md-12 column">
                                <div class="contact-section-btn">
                                    <div class="form-group style-two">                                       
                                        <input class="theme-btn btn-style-two" type="submit" name="submit" value="Submit Now" data-loading-text="Please wait...">
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </form>
                    <!--/ row -->

                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
           
    </section>
    <!--/ Announcment-->
    <!--/ page body -->

    <!--main sub page -->
    
   <?php include 'footer.php' ?>
</div>
<!--End pagewrapper-->
    

<!-- Scroll Top Button -->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-angle-up"></span>
</button>   

<?php include 'footerscripts.php' ?>

<script>
    $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
</script>

</body>
</html>