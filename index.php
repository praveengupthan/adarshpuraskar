<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Adarsh Puraskar</title>

<?php include 'headerstyles.php' ?>

<!-- Fav Icons -->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>
<div class="page-wrapper">    
    <!-- Preloader -->
    <div class="preloader"></div> 
   <?php include 'header.php' ?>

    <!-- Main slider -->
    <section class="main-slider">
        <div class="container-fluid">
            <ul class="main-slider-carousel owl-carousel owl-theme slide-nav">
                <li class="slider-wrapper">
                    <div class="image"><img src="images/main-slider/image-1.jpg" alt=""></div>
                    <div class="slider-caption light text-center">
                        <div class="container">
                            <h3>Aadarsh Puraskar   <span class="border-shape"></span></h3>
                            <h1>Award is confined to the States of Telangana, Andhra Pradesh and Maharastra for each succeeding year.</h1>
                            <div class="text light large-text">Justice M N Rao & Dr Shalini Rao Pargaonkar Foundation </div>
                            <!-- <div class="link-btn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div> -->
                        </div>                            
                    </div>
                    <div class="slide-overlay"></div>
                </li>
                <li class="slider-wrapper">
                    <div class="image"><img src="images/main-slider/image-2.jpg" alt=""></div>
                    <div class="slider-caption light text-center">
                        <div class="container">
                            <h3>Aadarsh Puraskar   <span class="border-shape"></span></h3>
                            <h1>“Aadarsh Puraskar” of Rs.5,00,000/- (Rupees five lakhs only) in cash and a citation annually to an individual which reflects in brief the service rendered and the excellence achieved by the awardee.</h1>
                            <div class="text light large-text">Justice M N Rao & Dr Shalini Rao Pargaonkar Foundation</div>
                            <!-- <div class="link-btn"><a href="#" class="theme-btn btn-style-two">Learn More</a></div> -->
                        </div>                          
                    </div>
                    <div class="slide-overlay"></div>
                </li>
                <li class="slider-wrapper">
                    <div class="image"><img src="images/main-slider/image-3.jpg" alt=""></div>
                    <div class="slider-caption light text-center">
                        <div class="container">
                        <h3>Aadarsh Puraskar   <span class="border-shape"></span></h3>
                            <h1>Foundation to recognise, reward unsung heroes</h1>
                            <div class="text light large-text">Justice M N Rao & Dr Shalini Rao Pargaonkar Foundation </div>
                            <!-- <div class="link-btn"><a href="http://timesofindia.indiatimes.com/articleshow/69909195.cms?utm_source=contentofinterest&utm_medium=text&utm_campaign=cppst" class="theme-btn btn-style-two" target="_blank">Learn More</a></div> -->
                        </div>                          
                    </div>
                    <div class="slide-overlay"></div>
                </li>
            </ul>
        </div>
    </section>   

    <!-- About Us -->
    <section class="about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 image-block-one">
                    <div class="image big-image"><img src="images/resource/about.jpg" alt=""></div>
                    <div class="image small-image"><img src="images/resource/about-small.jpg" alt=""></div>
                </div>
                <div class="col-lg-7 offset-lg-1 align-self-center">
                    <div class="pl-lg-5">
                        <!-- sec-title -->
                        <div class="sec-title style-two">
                            <h4>About the foundation</h4>
                            <h1>Thought behind  <span>Adarsh Puraskar</span></h1>
                        </div>
                        <!-- about block -->
                        <div class="about-block">
                            <div class="inner-box">
                                <div class="text">
                                    <p>With the vast experience gained, the Founder is of the opinion that the contemporary scene presents a bleak picture.
                                        Excellence is side tracked if not eclipsed. Recognition passes by eminent persons despite their achieving high degree of success.</p>
                                    <p>It is the duty of the society to restore the honour and recognition to those deservedly due. In this direction, several institutions and individuals have initiated measures.</p>
                                </div>
                                <div class="link-btn"><a href="about.php" class="theme-btn btn-style-four donate-box-btn">Read More</a></div>
                            </div>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
    </section>

    <!-- News Section -->
    <section class="news-section sp-two">
        <div class="container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <div class="icon-box"><img src="images/icons/sec-title.png" alt=""></div>
                <h1><span>Media</span></h1>
                <div class="text">Adarsh Puraskar News & Updates</div>
            </div>
            
            <div class="row clearfix justify-content-center">
                <!--News Block-->
                <div class="news-block-one col-lg-4 col-sm-6">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/resource/news-1.jpg" alt="">
                            <div class="overlay">
                                <a class="link-btn" href="https://timesofindia.indiatimes.com/city/hyderabad/foundation-to-recognise-reward-unsung-heroes/articleshow/69909195.cms" target="_blank">
                                    <i class="fa fa-link"></i>
                                </a>                                
                            </div>
                        </div>
                        <div class="lower-box">
                            <div class="date">22 <span>june</span><span>2019</span></div>
                            <div class="post-meta">By: <span> Adarsh Puraskar</span></div>
                            <h4><a href="https://timesofindia.indiatimes.com/city/hyderabad/foundation-to-recognise-reward-unsung-heroes/articleshow/69909195.cms" target="_blank">Sadbhavana Award to Justice MN Rao </a></h4>
                            <div class="text">It may be mentioned here that, Late Rajiv Gandhiji hoisted the Congress Party Flag at Charminar and commenced his Sadbhavana Yatra in twin Cities on this day in the year 1990. </div>
                            <div class="read-more-btn">
                                <a href="https://timesofindia.indiatimes.com/city/hyderabad/foundation-to-recognise-reward-unsung-heroes/articleshow/69909195.cms" class="read-more" target="_blank">Read More </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--News Block-->
                <div class="news-block-one col-lg-4 col-sm-6">
                    <div class="inner-box">
                        <div class="image">
                            <img src="images/resource/news-2.jpg" alt="">
                            <div class="overlay">
                                <a class="link-btn" href="https://indtoday.com/sadbhavana-award-to-justice-mn-rao/" target="_blank">
                                    <i class="fa fa-link"></i>
                                </a>                                
                            </div>
                        </div>
                        <div class="lower-box">
                            <div class="date">05 <span>Oct</span><span>2016</span></div>
                            <div class="post-meta">By: <span> ChariteMax</span></div>
                            <h4><a href="https://indtoday.com/sadbhavana-award-to-justice-mn-rao/" target="_blank">Sadbhavana Award to Justice MN Rao</a></h4>
                            <div class="text">It may be mentioned here that, Late Rajiv Gandhiji hoisted the Congress Party Flag at Charminar and commenced his Sadbhavana Yatra in twin Cities on this day in the year 1990.  </div>
                            <div class="read-more-btn">
                                <a href="https://indtoday.com/sadbhavana-award-to-justice-mn-rao/" class="read-more" target="_blank">Read More </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   <?php include 'footer.php' ?>
</div>
<!--End pagewrapper-->   

<!-- Scroll Top Button -->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-angle-up"></span>
</button>   
<?php include 'footerscripts.php' ?>

</body>
</html>