
    <!-- Main footer -->
    <footer class="main-footer">
        <div class="container">        
            <!--Widgets Section-->
            <div class="widgets-section">
                <div class="row clearfix">
                    <!--Big Column-->
                    <div class="big-column col-lg-6">
                        <div class="row clearfix">                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-7">
                                <div class="footer-widget about-widget">
                                    <!-- <div class="footer-logo">
                                        <figure>
                                            <a href="javascript:void(0)"><img src="images/footer-logo.png" alt=""></a>
                                        </figure>
                                    </div> -->
                                    <div class="widget-content">
                                        <div class="text">“Aadarsh Puraskar” presented annually to an individual which reflects in brief the service rendered and the excellence achieved by the awardee.
</div> 
                                    </div>
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="footer-column col-md-5">
                                <div class="footer-widget services-widget">
                                    <h2 class="widget-title">Organization</h2>
                                    <div class="widget-content">
                                        <ul class="list">
                                            <li><a href="index.php">Home</a></li>
                                            <li><a href="about.php">Foundation</a></li>
                                            <li><a href="announcement.php">Announcements</a></li>
                                            <li><a href="media.php">Media</a></li>
                                            <li><a href="contact.php">Contact</a></li>                                            
                                        </ul>
                                    </div>
                                </div>  
                            </div>                          
                        </div>
                    </div>  
                    <!--Footer Column-->
                    <div class="footer-column col-md-5">
                        <div class="footer-widget contact-widget">
                            <h2 class="widget-title">Contact</h2>
                            <div class="widget-content">
                                <ul class="contact-info-list">
                                    <li><i class="fas fa-map-marker-alt"></i>Flat no 313, Prithvi Block, My Home Nawadweepa, Hitech city, Hyderabad, Telangana</li>
                                    <li><i class="fas fa-phone"></i>Support: 9849297398</li>
                                    <li><i class="fas fa-envelope-open"></i>Email: jmnr.srpfdn@gmail.com</li>
                                </ul>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
        
</footer>