<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Adarsh Puraskar</title>

<?php include 'headerstyles.php' ?>

<!-- Fav Icons -->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>
<div class="page-wrapper">    
    <!-- Preloader -->
    <div class="preloader"></div> 
   <?php include 'header.php' ?>

    <!--main sub page -->
    <!--Page title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg)">
        <div class="container">
            <h1>About Us</h1>
        </div>
    </section>

    <div class="bread-crumb">
        <div class="container">
            <ul class="clearfix">
                <li><a href="index.php"><span class="fa fa-home"></span>Home</a></li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>
    <!--/ page title -->

    <!-- page body -->
    <div class="subpage">

     <!-- About Us -->
     <section class="about-us-two sp-four">
        <div class="container">
            <!-- about block -->
            <div class="about-block-two">
                <div class="row justify-content-center pb-4">
                    <div clss="col-lg-6">
                        <img src="images/resource/about-coupleimg.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="inner-box">
                    <h4>Justice M. N. Rao <span></span></h4>
                    <h1>About the <span class="theme-color">Adarsh Puraskar</span></h1>
                    <div class="text">
                        <p>With the vast experience gained, the Founder is of the opinion that the contemporary scene presents a bleak picture. Excellence is side tracked if not eclipsed. Recognition passes by eminent persons despite their achieving high degree of success. It is the duty of the society to restore the honour and recognition to those deservedly due. In this direction, several institutions and individuals have initiated measures. Answerability to the society is a concept, in the view of the Founder, which comprehends not only adherence to rigorous standards governing the different fields of human activity but also genuine efforts to repay the social debt which, in a large measure, reflects in encouraging recognition of those who achieved high degree of excellence without any self aggrandizement.</p>

                        <p>Society must honour those who enriched it; it is a debt that ought to be redeemed. To abide by this before the emptiness envelops corroding both his mental and physical faculties, he has constituted a Foundation in his name and in the name of his beloved wife late Dr.Smt.M.Shalini Rao Pargaonkar. </p>
                    </div>                   
                </div>                
            </div>
            <!--/ about block -->

            <!-- tab-->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home">Justice M.N.Rao</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1">Dr. Smt. Shalini Rao Pargaonkar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu2">Foundation Object</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu3">Selection Committee</a>
                </li>
             </ul>

            <!-- tab content -->
            <div class="tab-content">
                <!-- about the foundation-->
                <div id="home" class="container tab-pane active"><br>
                    <h3>About the Founder and the Foundation</h3>
                    <div class="row">                        
                        <div class="col-lg-6">
                        <p>Justice M. N. Rao was born on 22-04-1936 in Nellore, A.P. Obtained Master of Laws in Constitutional and International Law in 1961 from the Madras University under the guidance of Prof. Alexandrowicz, an internationally renowned Professor. Worked as Lecturer in the Osmania University in the faculties of Law and Arts during the academic year 1964-65. Taught Constitutional and International Law to LL.M., students and International Affairs to M.A. students.</p>
                    <p>His father late M.Subrahmanyam was a Town Planning Officer of the Nellore Municipality. His elder brother late Makani Venkata Rao had been into spiritual activities till he left this world and his younger brother late Makani Krishna Murthy (M.S., New York), was a structural engineer settled in the United States of America. His only son Mr.Gautam Makani completed his M.S., from USA and MBA from Wharton School of Business Management, USA. He is a citizen of the United States of America and presently, he is working in IBM in  a senior position. His daughter-in-law Dr.Mrs.Priti Gautam is a M.B.A., from Trinity College, Dublin and Ph.D from the University of California and is working as a consultant in the United States of America. His grandson, Soham Makani, is an under-graduate student in Engineering at the University of Illinois, USA. </p>
                    <p>Justice M.N.Rao practiced as an advocate in the High Court of Andhra Pradesh for 12 years. Appointed as District and Sessions Judge in 1973 by direct recruitment. Between 1979 and 1983, worked as Secretary to Government, Law and Legislative Affairs, Government of Andhra Pradesh. Deputed to work at the Institute of Advanced Legal Studies, London for six months under the Colombo Plan, a prestigious assignment. Architect of many innovative laws such as Abolition of Capitation fee, Equal Property Rights for Hindu daughters etc.</p>
                    <p>Appointed as permanent Judge of the High Court of Andhra Pradesh in July 1986. Delivered Prof.Venkataraman Endowment Lectures at the Andhra Pradesh University in 1991 on "Social Justice: Some Aspects". Also delivered Babasaheb Dr. B. R. Ambedkar Memorial Lecture at the Nagpur University in August, 1997. Delivered many important judgments in all branches of law especially Constitutional and International Law. He was a visiting Professor of the Nagpur University and an examiner for assessing doctoral theses for award of Ph.D.</p>
                        </div>
                        <div class="col-lg-6">
                        <p>Participated in many national and international seminars and delivered key-note addresses and presented papers. The lectures delivered and the papers presented by him were published in the form of three books titled “Random Reflections on Law and Allied Matters”, “Reflections on Law and Society” and “Readings in Sociology of Law”. Reviewing two of his books, Shri Justice V. R. Krishna Iyer, an eminent former Judge of the Supreme Court of India, has made the following observations:</p>
                    <ul class="py-3 pl-3">
                        <li class="pb-2">
                        * "The gamut of the subjects spread out is enviable and the treatment excellent. Our Judicial culture stands indebted to Justice Rao both for his contribution and the example he has set for others of his ilk not to be cocooned in paper books, forensic submissions and professional insulation."
                        </li>
                        <li>
                        * “Justice M.N.Rao is worth every page for serious reading. I content myself with a general observation: the pages are luminous although the book itself is not voluminous; the ideas are quite modern and the presentation is in clear diction.”
                        </li>
                    </ul>

                    <p>
                    Justice M.N. Rao was the Chief Justice of High Court of Himachal Pradesh from November 1997 to April 1998. Designated as a Senior Advocate, Shri Rao has practised as a Senior Advocate in the Supreme Court from 1-9-1998 to 5-6-2010. Appointed as Chairperson, National Commission for Backward Classes in June, 2010 with the status of Union Cabinet Minister and continued as such till 2013. After demitting the office of the Chairperson, National
                    Commission for Backward Classes, he resumed his practice in the Supreme Court as a Senior Advocate.
                    </p>
                        </div>
                    </div>
                    
                   
                </div>
                <!--/ abut the foundation-->
                <div id="menu1" class="container tab-pane fade"><br>
                    <h3>About Dr. Smt. Shalini Rao Pargaonkar</h3>
                    <p>Born on 11th January, 1938 in the well known and respected Pargaonkar family, Dr.Smt.M.Shalini Rao Pargaonkar had her early education in the reputed Saraswathi Bhuvan educational institutions, Aurangabad. She was an alumnus of the Osmania Medical College, Hyderabad, having pursued the medical course leading to the award of Degree of MBBS during the period 1955-60. Subsequently, she obtained a post graduate diploma in Medicine from the Madras University. For three decades from 1966 to 1996, she worked as Lady Medical Officer, Osmania University Dispensary and earned in good measure, the good will and respect of students and the staff – both teaching and non teaching - of the University Campus Colleges as well as the Women’s College, Koti. Dr.Smt.Shalini Rao Pargaonkar left for her heavenly abode on 14th March, 2017.</p>
                    <p>To perpetuate her memory, the Founder has instituted three scholarships in the Osmania Medical College, Koti, Hyderabad for three girl students aggregating to Rs.2 lakhs per year and scholarships for 12 girl students and cash prizes for three lady teachers every year in the Saraswathi Bhuvan Educational Institutions, Aurangabad, both the endowments with a total corpus fund of Rs.one crore.</p>
                </div>
                <div id="menu2" class="container tab-pane fade"><br>
                    <h3>Object of the Foundation</h3>
                    <p>The object of the Foundation is to award “Aadarsh Puraskar” of Rs.5,00,000/- (Rupees five lakhs only) in cash and a citation annually to an individual whose efforts and endeavours, without any personal aggrandizement, impacted the society resulting in enrichment of knowledge and conspicuous benefits – material and cultural - to the society. The citation reflects in brief the service rendered and the excellence achieved by the awardee. The area of selection of the person for the purpose of the Award is confined to the States of Telangana and Andhra Pradesh (both Telugu speaking States) and Maharastra for each succeeding year.</p>

                    <p>For the purpose of carrying out the above object, the Founder has set apart an amount of Rs.1,00,00,000/- (Rupees one crore only) from out of his personal savings as the Corpus Fund for the Foundation.</p>
                </div>
                <div id="menu3" class="container tab-pane fade"><br>
                    <h3>Selection Committee and procedure for selecting the awardee</h3>
                    <p>The Selection Committee for selecting the awardee comprises the following members:</p>
                    <ul>
                        <li>a) Justice M.N.Rao, Honorary Chairman.</li>
                        <li>b) Justice Challa Kodandaram, Chairman (Judge, High Court for the State of Telangana).</li>
                        <li>c) Mr.Gautam Makani</li>
                        <li>d) Mr.Yogesh S.Pargaonkar</li>
                        <li>e) Captain Atul A. Dharwadkar</li>
                        <li>f) Mr.Mukund Yeshwanth Rao Deshmukh,</li>
                        <li>g) Dr.Amir Ullah Khan</li>
                        <li>h) Dr.Amita Dhanda</li>
                        <li>i) Mr.Swaroop Oggu</li>
                        <li>j) Mr.M.Sree Rama Chandra Murthy</li>
                        <li>k) Mr.T.Venkateswara Rao</li>
                    </ul>
                    <p>The Committee, by advertisement in daily Newspapers – English and regional language, - invites applications from individuals and requests institutions to sponsor individuals who fall within the prarameters laid down above for selection of the person for award of the Aadarsh Puraskar.</p>
            
                </div>
            </div>          
            <!-- tab content -->

            <!--/ tab-->
        </div>
        </div>
    </section>
    <!--/ about us-->
    <!--/ page body -->

    <!--main sub page -->
    
   <?php include 'footer.php' ?>
</div>
<!--End pagewrapper-->
    

<!-- Scroll Top Button -->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-angle-up"></span>
</button>   

<?php include 'footerscripts.php' ?>

</body>
</html>