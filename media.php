<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Adarsh Puraskar</title>

<?php include 'headerstyles.php' ?>

<!-- Fav Icons -->
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<body>
<div class="page-wrapper">    
    <!-- Preloader -->
    <div class="preloader"></div> 
   <?php include 'header.php' ?>

    <!--main sub page -->
    <!--Page title-->
    <section class="page-title" style="background-image:url(images/background/5.jpg)">
        <div class="container">
            <h1>Media</h1>
        </div>
    </section>

    <div class="bread-crumb">
        <div class="container">
            <ul class="clearfix">
                <li><a href="index.php"><span class="fa fa-home"></span>Home</a></li>
                <li class="active">Media</li>
            </ul>
        </div>
    </div>
    <!--/ page title -->
    <!-- page body -->
    <div class="subpage">
        <section class="gallery-block grid-gallery">
            <div class="container">     
                
                <!-- tab here -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#articles">Articles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#videos">Videos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#photos">Photos</a>
                    </li>                    
                </ul>

                 <!-- tab content -->
                 <div class="tab-content">
                    <!-- articles -->
                    <div id="articles" class="container tab-pane active">
                         <div class="row clearfix pt-4">
                              <!--News Block-->
                                <div class="news-block-one col-lg-4 col-md-6">
                                    <div class="inner-box">
                                        <div class="image">
                                            <img src="images/resource/news-1.jpg" alt="">
                                            <div class="overlay">
                                                <a class="link-btn" href="https://timesofindia.indiatimes.com/city/hyderabad/foundation-to-recognise-reward-unsung-heroes/articleshow/69909195.cms" target="_blank">
                                                    <i class="fa fa-link"></i>
                                                </a>                                
                                            </div>
                                        </div>
                                        <div class="lower-box">
                                            <div class="date">22 <span>june</span><span>2019</span></div>
                                            <div class="post-meta">By: <span> Adarsh Puraskar</span></div>
                                            <h4><a href="https://timesofindia.indiatimes.com/city/hyderabad/foundation-to-recognise-reward-unsung-heroes/articleshow/69909195.cms" target="_blank">Sadbhavana Award to Justice MN Rao </a></h4>
                                            <div class="text">It may be mentioned here that, Late Rajiv Gandhiji hoisted the Congress Party Flag at Charminar and commenced his Sadbhavana Yatra in twin Cities on this day in the year 1990. </div>
                                            <div class="read-more-btn">
                                                <a href="https://timesofindia.indiatimes.com/city/hyderabad/foundation-to-recognise-reward-unsung-heroes/articleshow/69909195.cms" class="read-more" target="_blank">Read More </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ news block -->

                                <!--News Block-->
                                <div class="news-block-one col-lg-4 col-md-6">
                                    <div class="inner-box">
                                        <div class="image">
                                            <img src="images/resource/news-2.jpg" alt="">
                                            <div class="overlay">
                                                <a class="link-btn" href="https://indtoday.com/sadbhavana-award-to-justice-mn-rao/" target="_blank">
                                                    <i class="fa fa-link"></i>
                                                </a>                                
                                            </div>
                                        </div>
                                        <div class="lower-box">
                                            <div class="date">05 <span>Oct</span><span>2016</span></div>
                                            <div class="post-meta">By: <span> ChariteMax</span></div>
                                            <h4><a href="https://indtoday.com/sadbhavana-award-to-justice-mn-rao/" target="_blank">Sadbhavana Award to Justice MN Rao</a></h4>
                                            <div class="text">It may be mentioned here that, Late Rajiv Gandhiji hoisted the Congress Party Flag at Charminar and commenced his Sadbhavana Yatra in twin Cities on this day in the year 1990.  </div>
                                            <div class="read-more-btn">
                                                <a href="https://indtoday.com/sadbhavana-award-to-justice-mn-rao/" class="read-more" target="_blank">Read More </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ news block -->
                         </div>
                    </div>
                    <!--/ articles -->

                    <!-- videos tab -->
                    <div id="videos" class="container tab-pane">
                        <!-- row -->
                        <div class="row pt-4">
                            <!-- col -->
                            <div class="col-lg-4">
                                <iframe width="100%" height="320" src="https://www.youtube.com/embed/FCiUpj3eeTU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-lg-4">
                            <iframe width="100%" height="320" src="https://www.youtube.com/embed/pQ2zKKXPFsE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ videos tab -->

                    <!-- gallery tab -->
                    <div id="photos" class="container tab-pane">
                    <div class="row pt-4">
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/1.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/1.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/2.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/2.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/3.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/3.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/4.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/4.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/5.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/5.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/6.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/6.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/7.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/7.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/8.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/8.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/9.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/9.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/10.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/10.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/11.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/11.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/12.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/12.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/13.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/13.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/14.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/14.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/15.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/15.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/16.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/16.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/17.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/17.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/18.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/18.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/19.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/19.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/20.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/20.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/21.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/21.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/22.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/22.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/23.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/23.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/24.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/24.jpg">
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-3 item">
                            <a class="lightbox" href="images/gallery/25.jpg">
                                <img class="img-fluid image scale-on-hover" src="images/gallery/25.jpg">
                            </a>
                        </div> 
                </div>

                    </div>
                    <!--/ tallery tab -->
                 </div>
                 <!--/ tab content -->
                <!--/ tab ends -->
            </div>
        </section> 
    </div> 
    <!--/ page body -->
    <!--main sub page -->
    
   <?php include 'footer.php' ?>
</div>
<!--End pagewrapper-->
    

<!-- Scroll Top Button -->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-angle-up"></span>
</button>   

<?php include 'footerscripts.php' ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
</script>

</body>
</html>